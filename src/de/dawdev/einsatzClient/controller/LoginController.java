package de.dawdev.einsatzClient.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import de.dawdev.einsatzClient.connection.Client;
import de.dawdev.einsatzClient.database.LoginDB;
import de.dawdev.einsatzClient.model.FahrzeugModel;
import de.dawdev.einsatzClient.utils.Utils;
import de.dawdev.einsatzClient.view.LoginView;

public class LoginController implements ActionListener {
	private LoginView view;

	@SuppressWarnings("unused")
	private LoginController() {
	}

	public LoginController(LoginView loginView) {
		view = loginView;
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		String username, passwordHash;
		username = view.getUsername();
		passwordHash = Utils.hash(view.getPassword());
		if (LoginDB.isLoginValid(username, passwordHash)) {
			// Login correct
			Client c = Client.getClient();
			ArrayList<FahrzeugModel> fahrzeuge = c.getFahrzeuge();
			MainController.startMain(username, fahrzeuge);
			view.dispose();
		} else {
			JOptionPane.showConfirmDialog(null, "Login fehlgeschlagen.", null, JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE);
		}
	}

}
