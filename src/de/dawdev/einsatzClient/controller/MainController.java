package de.dawdev.einsatzClient.controller;

import java.util.ArrayList;

import javax.swing.SwingUtilities;

import de.dawdev.einsatzClient.model.FahrzeugModel;
import de.dawdev.einsatzClient.model.MainModel;
import de.dawdev.einsatzClient.view.MainView;

public class MainController {
	@SuppressWarnings("unused")
	private MainView mView;
	@SuppressWarnings("unused")
	private MainModel mModel;
	
	public static MainController startMain(String username, ArrayList<FahrzeugModel> fahrzeuge){
		MainController controller = new MainController();
		controller.mModel = new MainModel(username, fahrzeuge);
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				controller.mView = new MainView(fahrzeuge, controller);
			}
		});
		return controller;
	}
}
