package de.dawdev.einsatzClient.database;

import java.sql.*;

import de.dawdev.einsatzClient.utils.Logger;

public class Database {
	/**
	 * Gibt ein Connection-Objekt mit einer offenen Datenbankverbindung zur emm.db zur�ck.
	 * @return Connection-Objekt zur emm.db
	 */
	public static Connection getConnection(){
		Connection con = null;
		try {
			Class.forName("org.sqlite.JDBC");
			con = DriverManager.getConnection("jdbc:sqlite:data.db");
		} catch (SQLException e) {
			Logger.errorMsg("Datenbankverbindungsaufbau fehlgeschlagen!");
			e.printStackTrace();
			System.exit(0);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return con;
	}
}
