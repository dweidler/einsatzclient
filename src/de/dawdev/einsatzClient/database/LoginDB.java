package de.dawdev.einsatzClient.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import de.dawdev.einsatzClient.utils.Logger;

public class LoginDB {
	public static boolean isLoginValid(String username, String password){
		try {
			Connection c = Database.getConnection();
			String query = "SELECT * FROM logins WHERE username = ? AND password = ?";
			PreparedStatement stmt = c.prepareStatement(query);
			
			stmt.setString(1, username);
			stmt.setString(2, password);
			
			ResultSet rs = stmt.executeQuery();
			
			boolean isValid = false;
			while(rs.next()){
				isValid = true;
			}
			return isValid;
		} catch (SQLException e) {
			Logger.errorMsg("Executing isLoginValid()-Query failed: " + e.getMessage());
			e.printStackTrace();
		}
		return false;
	}
}
