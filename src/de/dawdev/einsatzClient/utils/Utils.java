package de.dawdev.einsatzClient.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Utils {
	public static String hash(String txt){
		MessageDigest sha512;
		try {
			sha512 = MessageDigest.getInstance("SHA-512");
			byte[] textBytes = txt.getBytes();
		    byte[] textHash = sha512.digest(textBytes);
		    return convertByteArrayToHexString(textHash);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}        
	    return "";
	}
	
	private static String convertByteArrayToHexString(byte[] arrayBytes) {
	    StringBuffer stringBuffer = new StringBuffer();
	    for (int i = 0; i < arrayBytes.length; i++) {
	        stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16)
	                .substring(1));
	    }
	    return stringBuffer.toString();
	}
	
	public static boolean isNumber(String str){
		for(int i = 0; i < str.length(); ++i){
			char toCheck = str.charAt(i);
			if(toCheck != '0' && toCheck != '1' && toCheck != '2' && toCheck != '3' && toCheck != '4' && toCheck != '5' && toCheck != '6' && toCheck != '7' && toCheck != '8' && toCheck != '9'){
				return false;
			}
		}
		return true;
	}
}
