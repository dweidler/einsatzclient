package de.dawdev.einsatzClient.utils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import de.dawdev.einsatzClient.Main;

public class Logger {
	private FileWriter mWriter;
	private static Logger mSingleton = null;	
	static SimpleDateFormat simpleDateFormatter = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss");
	
	private Logger(File file){
		try {
			mWriter = new FileWriter(file, true);
		} catch (IOException e) {
			System.out.println("Critical file writing exception");
			System.exit(0);
		}
	}
	
	private static void write(String msg){
		if(mSingleton == null) mSingleton = new Logger(new File("log.txt"));
		try {
			mSingleton.mWriter.write(msg);
			mSingleton.mWriter.flush();
		} catch (IOException e) {
			errorMsg("Logging failed and might be corrupted! Check if the server still takes logs or not and contact your helpdesk");
		}
	}
	
	public static void debugMsg(String msg) {
		if (!Main.isOnDebug()) return;
		msg = String.format("%s %-10s%s%n", simpleDateFormatter.format(new Date()), "[DEBUG]:", msg);
		System.out.print(msg);
		write(msg);
	}

	public static void errorMsg(String msg) {
		msg = String.format("%s %-10s%s%n", simpleDateFormatter.format(new Date()), "[ERROR]:", msg);
		System.out.print(msg);
		write(msg);
	}

	public static void infoMsg(String msg) {
		msg = String.format("%s %-10s%s%n", simpleDateFormatter.format(new Date()), "[INFO]:", msg);
		System.out.print(msg);
		write(msg);
	}
}
