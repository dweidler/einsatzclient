package de.dawdev.einsatzClient.model;

import java.util.ArrayList;

public class MainModel {
	private String mUsername;
	private ArrayList<FahrzeugModel> mFahrzeuge;

	public String getUsername() {
		return mUsername;
	}

	public void setUsername(String username) {
		mUsername = username;
	}

	public ArrayList<FahrzeugModel> getFahrzeuge() {
		return mFahrzeuge;
	}

	public void setFahrzeuge(ArrayList<FahrzeugModel> fahrzeuge) {
		mFahrzeuge = fahrzeuge;
	}
	
	public MainModel(String username, ArrayList<FahrzeugModel> fahrzeuge){
		setUsername(username);
		setFahrzeuge(fahrzeuge);
	}
	
	private MainModel(){
		
	}
}
