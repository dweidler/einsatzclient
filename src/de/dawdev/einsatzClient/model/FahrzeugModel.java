package de.dawdev.einsatzClient.model;

public class FahrzeugModel {
	private int mId;
	private String mBezeichnung;
	
	public FahrzeugModel() {
		// TODO Auto-generated constructor stub
	}
	
	public FahrzeugModel(int id, String bezeichnung) {
		mId = id;
		mBezeichnung = bezeichnung;
	}

	public int getId() {
		return mId;
	}

	public void setId(int id) {
		mId = id;
	}

	public String getBezeichnung() {
		return mBezeichnung;
	}

	public void setBezeichnung(String bezeichnung) {
		mBezeichnung = bezeichnung;
	}
}
