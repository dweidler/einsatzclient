package de.dawdev.einsatzClient;

import javax.swing.SwingUtilities;
import de.dawdev.einsatzClient.view.LoginView;

public class Main {
	public static boolean mDebug = true;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new LoginView();
			}
		});
	}

	public static boolean isOnDebug() {
		return mDebug;
	}

}
