package de.dawdev.einsatzClient.connection;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

import de.dawdev.einsatzClient.model.FahrzeugModel;
import de.dawdev.einsatzClient.utils.ErrorDialogs;
import de.dawdev.einsatzClient.utils.Logger;
import de.dawdev.einsatzClient.utils.Utils;

public class Client {
	private Socket mSocket;
	private static Client mInstance = null;
	private Client() {
		try {
			mSocket = new Socket("dorianpi.fritz.box", 1337);
		} catch (IOException e) {
			Logger.errorMsg("Konnte keine Verbindung zum Server aufbauen");
			System.exit(0);
		}
	}
	
	public static Client getClient(){
		if(mInstance == null)
			mInstance = new Client();
		return mInstance;
	}
	
	public ArrayList<FahrzeugModel> getFahrzeuge(){
		ArrayList<FahrzeugModel> fahrzeuge = new ArrayList<FahrzeugModel>();
		try {
			PrintWriter output = new PrintWriter(mSocket.getOutputStream());
			Scanner input = new Scanner(mSocket.getInputStream());
			output.println("GetAlleAutos");
			output.flush();
			while(input.hasNextLine()){
				String line = input.nextLine();
				if(line.startsWith("EndeFahrzeuge"))
					break;
				Scanner lineScanner = new Scanner(line);
				if(lineScanner.next().startsWith("Fahrzeug")){
					String id = lineScanner.next();
					String bezeichnung = lineScanner.next();
					if(Utils.isNumber(id)){
						fahrzeuge.add(new FahrzeugModel(Integer.valueOf(id), bezeichnung));
					}
				lineScanner.close();
				} else{
					break;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		for(FahrzeugModel fahrzeug : fahrzeuge){
			System.out.println(fahrzeug.getId() + "  -  " + fahrzeug.getBezeichnung());
		}
		return fahrzeuge;
	}
	
	public void addAuto(int id){
		PrintWriter output;
		try {
			output = new PrintWriter(mSocket.getOutputStream());
			output.println("AddAuto " + id);
			output.flush();
		} catch (IOException e) {
			ErrorDialogs.ConnectionDroppedMsg();
			System.exit(0);
		}
		
	}
}
