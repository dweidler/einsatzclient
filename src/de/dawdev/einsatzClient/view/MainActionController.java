package de.dawdev.einsatzClient.view;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import de.dawdev.einsatzClient.connection.Client;
import de.dawdev.einsatzClient.model.FahrzeugModel;

public class MainActionController implements ActionListener {

	private FahrzeugModel mFahrzeug;
	
	public MainActionController(FahrzeugModel fahrzeug) {
		mFahrzeug = fahrzeug;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Client.getClient().addAuto(mFahrzeug.getId());
	}

}
