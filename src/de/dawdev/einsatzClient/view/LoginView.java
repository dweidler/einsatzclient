package de.dawdev.einsatzClient.view;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.JButton;

import de.dawdev.einsatzClient.controller.LoginController;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class LoginView extends JFrame {
	private static final long serialVersionUID = 5108581869895584424L;
	private JTextField txtUsername;
	private JPasswordField txtPassword;

	private LoginController controller;

	public LoginView() {
		// GUI Design
		setTitle("Login");
		getContentPane().setLayout(null);

		JLabel lblUsername = new JLabel("Username");
		lblUsername.setBounds(10, 11, 59, 14);
		getContentPane().add(lblUsername);

		txtUsername = new JTextField();
		txtUsername.setBounds(79, 8, 148, 20);
		getContentPane().add(txtUsername);
		txtUsername.setColumns(10);

		JLabel lblPassword = new JLabel("Password");
		lblPassword.setBounds(10, 39, 59, 14);
		getContentPane().add(lblPassword);

		txtPassword = new JPasswordField();
		txtPassword.setColumns(10);
		txtPassword.setBounds(79, 36, 148, 20);
		getContentPane().add(txtPassword);

		JButton btnLogin = new JButton("Login");
		this.getRootPane().setDefaultButton(btnLogin);

		btnLogin.setBounds(10, 64, 118, 23);
		getContentPane().add(btnLogin);

		JButton btnClear = new JButton("Clear");

		btnClear.setBounds(138, 64, 89, 23);
		getContentPane().add(btnClear);
		this.setSize(250, 136);
		this.setLocationRelativeTo(null);
		this.setVisible(true);

		// Logic
		controller = new LoginController(this);
		this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		// Set Actions
		btnLogin.addActionListener(controller);
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				txtUsername.setText("");
				txtPassword.setText("");
			}
		});
	}

	public String getUsername() {
		return txtUsername.getText();
	}

	public String getPassword() {
		return String.valueOf(txtPassword.getPassword());
	}
}
