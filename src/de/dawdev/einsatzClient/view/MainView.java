package de.dawdev.einsatzClient.view;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.FlowLayout;
import java.util.ArrayList;

import javax.swing.JButton;

import de.dawdev.einsatzClient.controller.MainController;
import de.dawdev.einsatzClient.model.FahrzeugModel;

public class MainView extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7910228351091845157L;
	private JPanel contentPane;

	/**
	 * Create the frame.
	 */
	public MainView(ArrayList<FahrzeugModel> fahrzeuge, MainController controller) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(100, 100, 213, 176);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		JButton[] buttons = new JButton[fahrzeuge.size()];
		for(int i = 0; i < fahrzeuge.size(); i++){
			buttons[i] = new JButton(fahrzeuge.get(i).getBezeichnung());
			buttons[i].addActionListener(new MainActionController(fahrzeuge.get(i)));
			contentPane.add(buttons[i]);
		}
		this.pack();
		this.setLocationRelativeTo(null);
		this.setVisible(true);
	}
	
	@SuppressWarnings("unused")
	private MainView(){}

}
